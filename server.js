const express = require('express')
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();
const port = 3000;

app.use(cors());

// Configuring body parser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/sale', (req, res) => {
    res.json({
        CardCode: "C900001",
        DocumentStatus : "bost_Open",
        DocumentLines: [
            {
                ItemCode: "PG-CL-I-2",
                Quantity: "1",
                TaxCode: "UT",
                BatchNumbers: [
                    {
                        BatchNumber: "PG-CL-I-2-B-1",
                        Quantity: "1"
                    }
                ]
            },
        ]
    });
})
app.post('/print_sales', (req, res) => {
    console.log(req.body)
});

app.listen(port, () => console.log(`Retail app listening on port ${port}!`));